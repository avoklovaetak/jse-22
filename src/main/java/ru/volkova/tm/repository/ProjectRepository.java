package ru.volkova.tm.repository;

import ru.volkova.tm.api.repository.IProjectRepository;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.exception.empty.EmptyNameException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;

import java.util.Optional;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @Override
    public Optional<Project> add(
            final String userId,
            final String name,
            final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        return add(project);
    }

}
