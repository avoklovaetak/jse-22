package ru.volkova.tm.repository;

import ru.volkova.tm.api.repository.IUserRepository;
import ru.volkova.tm.entity.User;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public List<User> findAll() {
        return entities;
    }

    @Override
    public Optional<User> findByEmail(final String email) {
        return entities.stream()
                .filter(predicateByEmail(email))
                .findFirst();
    }

    @Override
    public Optional<User> findById(final String id) {
        return entities.stream()
                .filter(predicateById(id))
                .findFirst();
    }

    @Override
    public Optional<User> findByLogin(final String login) {
        return entities.stream()
                .filter(predicateByLogin(login))
                .findFirst();
    }

    @Override
    public void lockByEmail(String email) {
        findByEmail(email).ifPresent(user -> user.setLocked(false));
    }

    @Override
    public void lockById(String id) {
        findById(id).ifPresent(user -> user.setLocked(false));
    }

    @Override
    public void lockByLogin(String login) {
        findByLogin(login).ifPresent(user -> user.setLocked(false));
    }

    protected final Predicate<User> predicateByEmail(String email) {
        return user -> email.equals(user.getEmail());
    }

    protected final Predicate<User> predicateByLogin(String login) {
        return user -> login.equals(user.getLogin());
    }

    @Override
    public void remove(final User user) {
        entities.remove(user);
    }

    @Override
    public void removeByEmail(String email) {
        findByEmail(email).ifPresent(this::remove);
    }

    @Override
    public void removeById(final String id) {
        findById(id).ifPresent(this::remove);
    }

    @Override
    public void removeByLogin(final String login) {
        findByLogin(login).ifPresent(this::remove);
    }

    @Override
    public void unlockByEmail(String email) {
        findByEmail(email).ifPresent(user -> user.setLocked(true));
    }

    @Override
    public void unlockById(String id) {
        findById(id).ifPresent(user -> user.setLocked(true));
    }

    @Override
    public void unlockByLogin(String login) {
        findByLogin(login).ifPresent(user -> user.setLocked(true));
    }

}
