package ru.volkova.tm.repository;

import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public Optional<E> add(final E entity) {
        entities.add(entity);
        return Optional.ofNullable(entity);
    }

    protected final Predicate<E> predicateById(String id) {
        return e -> id.equals(e.getId());
    }

    @Override
    public void remove(final E entity) {
        entities.remove(entity);
    }

}
