package ru.volkova.tm.repository;

import ru.volkova.tm.api.repository.ITaskRepository;
import ru.volkova.tm.entity.Task;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public Optional<Task> add(
            final String userId,
            final String name,
            final String description
    ) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(task);
    }

    @Override
    public void bindTaskByProjectId(
            final String userId,
            final String projectId,
            final String taskId
    ) {
        findById(userId, taskId).ifPresent(task -> task.setProjectId(projectId));
    }

    @Override
    public List<Task> findAllByProjectId(
            final String userId,
            final String projectId
    ) {
        return entities.stream()
                .filter(predicateByUserIdAndProjectId(userId, projectId))
                .collect(Collectors.toList());
    }

    protected final Predicate<Task> predicateByUserIdAndProjectId(
            final String userId,
            final String projectId
    ) {
        return task -> projectId.equals(task.getProjectId())
                && userId.equals(task.getUserId());
    }

    @Override
    public void removeAllByProjectId(
            final String userId,
            final String projectId
    ) {
        findAllByProjectId(userId, projectId).forEach(this::remove);
    }

    @Override
    public void unbindTaskByProjectId(
            final String userId,
            final String projectId,
            final String taskId
    ) {
        findById(userId, taskId).ifPresent(task -> task.setProjectId(null));
    }

}
