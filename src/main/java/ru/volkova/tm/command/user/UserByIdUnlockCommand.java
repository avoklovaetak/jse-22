package ru.volkova.tm.command.user;

import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.util.TerminalUtil;

public class UserByIdUnlockCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "unlock user by id";
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockById(id);
        System.out.println("[OK]");
    }

    @Override
    public String name() {
        return "user-unlock-by-id";
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
