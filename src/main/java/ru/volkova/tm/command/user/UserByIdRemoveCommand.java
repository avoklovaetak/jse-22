package ru.volkova.tm.command.user;

import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.util.TerminalUtil;

public class UserByIdRemoveCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "lock remove by id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeById(id);
        System.out.println("[OK]");
    }

    @Override
    public String name() {
        return "user-remove-by-id";
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
