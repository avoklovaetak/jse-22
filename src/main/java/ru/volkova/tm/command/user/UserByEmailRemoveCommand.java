package ru.volkova.tm.command.user;

import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.util.TerminalUtil;

public class UserByEmailRemoveCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "remove user by email";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByEmail(email);
        System.out.println("[OK]");
    }

    @Override
    public String name() {
        return "user-remove-by-email";
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
