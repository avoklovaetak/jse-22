package ru.volkova.tm.command.user;

import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.util.TerminalUtil;

public class UserByIdLockCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "lock user by id";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockById(id);
        System.out.println("[OK]");
    }

    @Override
    public String name() {
        return "user-lock-by-id";
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
