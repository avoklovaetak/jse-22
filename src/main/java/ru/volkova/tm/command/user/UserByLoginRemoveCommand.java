package ru.volkova.tm.command.user;

import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.util.TerminalUtil;

public class UserByLoginRemoveCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "remove user by login";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
        System.out.println("[OK]");
    }

    @Override
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
