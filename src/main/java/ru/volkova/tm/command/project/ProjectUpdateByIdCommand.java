package ru.volkova.tm.command.project;

import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "update project by id";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        final Optional<Project> project = serviceLocator.getProjectService().findById(userId, id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Optional<Project> projectUpdated = serviceLocator.getProjectService()
                .updateOneById(userId, id, name, description);
        if (!projectUpdated.isPresent()) throw new ProjectNotFoundException();
    }

    @Override
    public String name() {
        return "project-update-by-id";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
