package ru.volkova.tm.command.project;

import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectStartByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "start project by name";
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        final Optional<Project> project = serviceLocator.getProjectService().startOneByName(userId, name);
        if (!project.isPresent()) throw new ProjectNotFoundException();
    }

    @Override
    public String name() {
        return "project-start-by-name";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
