package ru.volkova.tm.command.project;

import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "start project by index";
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = serviceLocator.getAuthService().getUserId();
        final Optional<Project> project = serviceLocator.getProjectService().startOneByIndex(userId, index);
        if (!project.isPresent()) throw new ProjectNotFoundException();
    }

    @Override
    public String name() {
        return "project-start-by-index";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
