package ru.volkova.tm.command.project;

import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.entity.Project;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "show project by id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        final Optional<Project> project = serviceLocator.getProjectService().findById(userId, id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        showProject(project.get());
    }

    @Override
    public String name() {
        return "project-show-by-id";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
