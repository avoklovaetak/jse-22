package ru.volkova.tm.command.authorization;

import ru.volkova.tm.entity.User;
import ru.volkova.tm.enumerated.Role;

import java.util.Optional;

public class UserViewProfileCommand extends AbstractAuthCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "view user profile";
    }

    @Override
    public void execute() {
        System.out.println("[VIEW PROFILE]");
        final Optional<User> user = serviceLocator.getAuthService().getUser();
        System.out.println("LOGIN: " + user.get().getLogin());
        System.out.println("E-MAIL: " + user.get().getEmail());
        System.out.println("FIRST NAME: " + user.get().getFirstName());
        System.out.println("LAST NAME: " + user.get().getSecondName());
        System.out.println("MIDDLE NAME" + user.get().getMiddleName());
    }

    @Override
    public String name() {
        return "user-view-profile";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
