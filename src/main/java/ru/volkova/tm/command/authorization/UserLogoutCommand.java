package ru.volkova.tm.command.authorization;

import ru.volkova.tm.enumerated.Role;

public class UserLogoutCommand extends AbstractAuthCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "log out of system";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

    @Override
    public String name() {
        return "user-logout";
    }

}
