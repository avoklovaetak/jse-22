package ru.volkova.tm.command.authorization;

import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractAuthCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "update information about user";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROFILE]");
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER FIRST NAME: ");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME: ");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME: ");
        final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    public String name() {
        return "user-update-profile";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
