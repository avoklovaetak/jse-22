package ru.volkova.tm.command.system;

import ru.volkova.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandsListCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "show program commands";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) {
            final String name = command.name();
            if (name == null) continue;
            System.out.println(name);
        }
    }

    @Override
    public String name() {
        return "commands-list";
    }

}
