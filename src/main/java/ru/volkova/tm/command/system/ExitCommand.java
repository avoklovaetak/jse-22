package ru.volkova.tm.command.system;

import ru.volkova.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "close application";
    }

    @Override
    public void execute() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

    @Override
    public String name() {
        return "exit";
    }

}
