package ru.volkova.tm.command.system;

import ru.volkova.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "show application version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("2.0.0");
    }

    @Override
    public String name() {
        return "version";
    }

}
