package ru.volkova.tm.command.system;

import ru.volkova.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "show developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Ekaterina Volkova");
        System.out.println("E-MAIL: avoklovaetak@yandex.ru");
    }

    @Override
    public String name() {
        return "about";
    }

}
