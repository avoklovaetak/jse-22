package ru.volkova.tm.command.task;

import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "update task by index";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER INDEX:");
        final String userId = serviceLocator.getAuthService().getUserId();
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Optional<Task> task = serviceLocator.getTaskService().findOneByIndex(userId, index);
        if (!task.isPresent()) throw new TaskNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Optional<Task> taskUpdated = serviceLocator.getTaskService()
                .updateOneByIndex(userId, index, name, description);
        if (!taskUpdated.isPresent()) throw new TaskNotFoundException();
    }

    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
