package ru.volkova.tm.command.task;

import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.util.TerminalUtil;

public class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "remove task by id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER ID:");
        final String userId = serviceLocator.getAuthService().getUserId();
        final String id = TerminalUtil.nextLine();
        serviceLocator.getTaskService().removeById(userId, id);
    }

    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
