package ru.volkova.tm.command.task;

import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "remove task by name";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getTaskService().removeOneByName(userId, name);
    }

    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
