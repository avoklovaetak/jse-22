package ru.volkova.tm.command.task;

import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.entity.TaskNotFoundException;
import ru.volkova.tm.entity.Task;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "update task by id";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getUserId();
        final Optional<Task> task = serviceLocator.getTaskService().findById(userId, id);
        if (!task.isPresent()) throw new TaskNotFoundException();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Optional<Task> taskUpdated = serviceLocator.getTaskService()
                .updateOneById(userId, id, name, description);
        if (!taskUpdated.isPresent()) throw new TaskNotFoundException();
    }

    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}