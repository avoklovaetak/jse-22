package ru.volkova.tm.exception.empty;

import ru.volkova.tm.command.AbstractCommand;
import ru.volkova.tm.exception.AbstractException;

public class EmptyCommandException extends AbstractException {

    public EmptyCommandException() {
        super("Error! Command is empty...");
    }

}
