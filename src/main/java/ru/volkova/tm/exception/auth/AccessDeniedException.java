package ru.volkova.tm.exception.auth;

import ru.volkova.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Access denied exception...");
    }

}
