package ru.volkova.tm.api.repository;

import ru.volkova.tm.entity.Project;

import java.util.Optional;

public interface IProjectRepository extends IOwnerRepository<Project> {

    Optional<Project> add(String userId, String name, String description);

}
