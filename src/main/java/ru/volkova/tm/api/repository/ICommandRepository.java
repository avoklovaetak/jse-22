package ru.volkova.tm.api.repository;

import ru.volkova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(AbstractCommand command);

    Collection<AbstractCommand> getArguments();

    AbstractCommand getCommandByArg(String arg);

    AbstractCommand getCommandByName(String name);

    Collection<AbstractCommand> getCommands();

}
