package ru.volkova.tm.api.repository;

import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.entity.User;

import java.util.List;
import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    void clear();

    List<User> findAll();

    Optional<User> findByEmail(String email);

    Optional<User> findById(String id);

    Optional<User> findByLogin(String login);

    void lockByEmail(String email);

    void lockById(String id);

    void lockByLogin(String login);

    void removeByEmail(String email);

    void removeById(String id);

    void removeByLogin(String login);

    void unlockByEmail(String email);

    void unlockById(String id);

    void unlockByLogin(String login);

}
