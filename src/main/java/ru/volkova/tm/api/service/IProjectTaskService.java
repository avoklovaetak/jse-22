package ru.volkova.tm.api.service;

import ru.volkova.tm.entity.Project;
import ru.volkova.tm.entity.Task;

import java.util.List;

public interface IProjectTaskService {

    void bindTaskByProjectId(String userId, String projectId, String taskId);

    List<Task> findAllTasksByProjectId(String userId, String projectId);

    void removeProjectById(String userId, String id);

    void unbindTaskByProjectId(String userId, String projectId, String taskId);

}
