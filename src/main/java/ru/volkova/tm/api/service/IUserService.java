package ru.volkova.tm.api.service;

import ru.volkova.tm.api.IService;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.enumerated.Role;

import java.util.List;
import java.util.Optional;


public interface IUserService extends IService<User> {

    void clear();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    List<User> findAll();

    Optional<User> findById(String id);

    Optional<User> findByLogin(String login);

    boolean isEmailExists(String email);

    boolean isLoginExists(String login);

    void lockByEmail(String email);

    void lockById(String id);

    void lockByLogin(String login);

    void remove(User user);

    void removeByEmail(String email);

    void removeById(String id);

    void removeByLogin(String login);

    Optional<User> setPassword(String userId, String password);

    void unlockByEmail(String email);

    void unlockById(String id);

    void unlockByLogin(String login);

    Optional<User> updateUser(
            final String userId,
            final String firstName,
            final String secondName,
            final String middleName
    );

}
