package ru.volkova.tm.api.service;

public interface ILogService {

    void command(String message);

    void debug(String message);

    void error(Exception e);

    void info(String message);

}
