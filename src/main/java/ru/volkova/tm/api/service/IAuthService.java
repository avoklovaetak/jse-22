package ru.volkova.tm.api.service;

import ru.volkova.tm.entity.User;
import ru.volkova.tm.enumerated.Role;

import java.util.Optional;

public interface IAuthService {

    void checkRoles(Role... roles);

    Optional<User> getUser();

    String getUserId();

    boolean isAuth();

    void login(String login, String password);

    void logout();

    void registry(String login, String password, String email);

}
