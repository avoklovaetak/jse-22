package ru.volkova.tm.api.service;

import ru.volkova.tm.entity.Task;

import java.util.Optional;

public interface ITaskService extends IOwnerService<Task> {

    Optional<Task> add(String userId, String name, String description);

}
