package ru.volkova.tm.api.service;

import ru.volkova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    void add(AbstractCommand command);

    Collection<AbstractCommand> getArguments();

    AbstractCommand getCommandByArg(String arg);

    AbstractCommand getCommandByName(String name);

    Collection<AbstractCommand> getCommands();

}
