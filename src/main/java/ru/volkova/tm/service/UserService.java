package ru.volkova.tm.service;

import ru.volkova.tm.api.repository.IUserRepository;
import ru.volkova.tm.api.service.IUserService;
import ru.volkova.tm.entity.User;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.auth.EmailExistsException;
import ru.volkova.tm.exception.auth.LoginExistsException;
import ru.volkova.tm.exception.empty.*;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.UserNotFoundException;
import ru.volkova.tm.util.HashUtil;

import java.util.List;
import java.util.Optional;

public final class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public void clear() {
        userRepository.clear();
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        userRepository.add(user);
        return user;
    }

    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (isEmailExists(email)) throw new EmailExistsException();
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    public Optional<User> findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    public Optional<User> findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public Optional<User> findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isEmailExists(final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email).isPresent();
    }

    @Override
    public boolean isLoginExists(final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login).isPresent();
    }

    @Override
    public void lockByEmail(String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        userRepository.lockByEmail(email);
    }

    @Override
    public void lockById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.lockById(id);
    }

    @Override
    public void lockByLogin(String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.lockByLogin(login);
    }

    @Override
    public void remove(final User user) {
        if (user == null) throw new ObjectNotFoundException();
        userRepository.remove(user);
    }

    @Override
    public void removeByEmail(String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        userRepository.removeByEmail(email);
    }

    @Override
    public void removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.removeById(id);
    }

    @Override
    public void removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.removeByLogin(login);
    }

    public Optional<User> setPassword(final String userId, final String password) {
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        final String hash = HashUtil.salt(password);
        user.get().setPasswordHash(hash);
        return user;
    }

    @Override
    public void unlockByEmail(String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        userRepository.unlockByEmail(email);
    }

    @Override
    public void unlockById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        userRepository.unlockById(id);
    }

    @Override
    public void unlockByLogin(String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.unlockByLogin(login);
    }

    public Optional<User> updateUser(
            final String userId,
            final String firstName,
            final String secondName,
            final String middleName
    ) {
        final Optional<User> user = findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        user.get().setFirstName(firstName);
        user.get().setSecondName(secondName);
        user.get().setMiddleName(middleName);
        return user;
    }

}
