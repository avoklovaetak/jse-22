package ru.volkova.tm.service;

import ru.volkova.tm.api.repository.IProjectRepository;
import ru.volkova.tm.api.service.IProjectService;
import ru.volkova.tm.entity.Project;

import java.util.Optional;

public final class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public Optional<Project> add(final String userId, final String name, final String description) {
        return projectRepository.add(userId, name, description);
    }

}
